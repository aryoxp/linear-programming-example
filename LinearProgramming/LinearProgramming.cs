using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LinearProgramming
{
    public partial class FormLinearProgramming : Form
    {
        public FormLinearProgramming()
        {
            InitializeComponent();
            InitValue();
        }

        #region Initial Value
        private void InitValue()
        {
            tEquation.Text = "";
            tbOutput.Clear();
            radioAR0.Checked = true;

            List<string> y = new List<string>();
            y.Add("15.1359");
            y.Add("19.9484");
            y.Add("23.6323");
            y.Add("30.5737");
            y.Add("35.2434");
            y.Add("40.1142");
            y.Add("44.1721");
            y.Add("49.8640");
            y.Add("55.5294");
            y.Add("59.8715");
            tbY.Lines = y.ToArray();

            List<string> x1 = new List<string>();
            x1.Add("4");
            x1.Add("8");
            x1.Add("2");
            x1.Add("5");
            x1.Add("3");
            x1.Add("8");
            x1.Add("1");
            x1.Add("8");
            x1.Add("6");
            x1.Add("7");
            tbX1.Lines = x1.ToArray();

            List<string> x2 = new List<string>();
            x2.Add("1");
            x2.Add("2");
            x2.Add("3");
            x2.Add("4");
            x2.Add("5");
            x2.Add("6");
            x2.Add("7");
            x2.Add("8");
            x2.Add("9");
            x2.Add("10");
            tbX2.Lines = x2.ToArray();
        }
        #endregion

        #region Debug Function
        public void Debug(Matrix m, String name) 
        {
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + name + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += m.ToString() + "\r\n\r\n";

        }
        #endregion

        #region Clean Up Array of String
        private String[] CleanUp(String[] A)
        {
            List<String> l = new List<string>();
            foreach (String s in A)
            {
                String temp = "";
                temp = s.Trim();
                if (temp.Length > 0) l.Add(temp);
            }
            return l.ToArray();
        }
        #endregion

        private void btCalculate_Click(object sender, EventArgs e)
        {
            tbOutput.Clear();

            tbY.Lines   = CleanUp(tbY.Lines);
            tbX1.Lines  = CleanUp(tbX1.Lines);
            tbX2.Lines  = CleanUp(tbX2.Lines);

            Int32 panjangY = tbY.Lines.Length;
            Int32 panjangX = tbX1.Lines.Length;

            Int32 AR = 0;
            if (radioAR1.Checked) AR = 1;
            if (radioAR2.Checked) AR = 2;
            if (radioAR3.Checked) AR = 3;

            try
            {
                if (AR == 0 && tbX1.Lines.Length != tbX2.Lines.Length) 
                    throw new Exception("Jumlah data X1 dan X2 harus sama.");

                if (tbX1.Lines.Length != tbY.Lines.Length)
                    throw new Exception("Jumlah data X dan Y harus sama.");

                if (tbX1.Lines.Length < (AR + 1))
                    throw new Exception("Jumlah data X dan Y kurang.");

                Matrix y = new Matrix(panjangY - AR, 1);
                Int32 j = 0;
                for (Int16 i = 0; i < panjangY; i++)
                {
                    if (i < AR) continue;
                    y[j, 0] = Double.Parse(tbY.Lines[i]);
                    j++;
                }
                Debug(y, "Matrix Y");

                Int32 col = 3;
                if (AR > 0) col = (AR - 1) + col;

                Matrix x = new Matrix(panjangX - AR, col);
                j = 0;
                for (Int16 i = 0; i < panjangX; i++)
                {
                    if (AR == 1 && i < AR) continue;
                    if (AR == 2 && i < AR) continue;
                    if (AR == 3 && i < AR) continue;

                    x[j, 0] = 1;

                    switch (AR)
                    {
                        case 0:
                            x[j, 1] = Double.Parse(tbX1.Lines[i]);
                            x[j, 2] = Double.Parse(tbX2.Lines[i]);
                            break;
                        case 1:
                            x[j, 1] = Double.Parse(tbX1.Lines[i]);
                            x[j, 2] = Double.Parse(tbX1.Lines[i - 1]);
                            break;
                        case 2:
                            x[j, 1] = Double.Parse(tbX1.Lines[i]);
                            x[j, 2] = Double.Parse(tbX1.Lines[i - 1]);
                            x[j, 3] = Double.Parse(tbX1.Lines[i - 2]);
                            break;
                        case 3:
                            x[j, 1] = Double.Parse(tbX1.Lines[i]);
                            x[j, 2] = Double.Parse(tbX1.Lines[i - 1]);
                            x[j, 3] = Double.Parse(tbX1.Lines[i - 2]);
                            x[j, 4] = Double.Parse(tbX1.Lines[i - 3]);
                            break;
                    }
                    j++;
                }
                Debug(x, "Matrix X");

                Matrix xTranspose = Matrix.Transpose(x);
                Debug(xTranspose, "Matrix X'");

                Matrix xTransposeX = Matrix.Multiply(xTranspose, x);
                Debug(xTransposeX, "Matrix X'.X");

                Matrix xTransposeXInverse = Matrix.Inverse(xTransposeX);
                Debug(xTransposeXInverse, "Matrix (X'.X)^-1");

                Matrix xTransposeY = Matrix.Multiply(xTranspose, y);
                Debug(xTransposeY, "Matrix X'.Y");

                Matrix beta = Matrix.Multiply(xTransposeXInverse, xTransposeY);
                Debug(beta, "Matrix beta = (X'.X)^-1 . (X'.Y)");

                String[] var = new String[] { "", "x1", "x2", "x3", "x4" };
                String equation = "   Y = ";

                for (Int16 i = 0; i < beta.NoRows; i++)
                {
                    equation += String.Format("{0:0.##}", beta[i, 0]) + var[i];
                    if (i < beta.NoRows - 1) equation += " + ";
                }

                tEquation.Text = equation;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btClear_Click(object sender, EventArgs e)
        {
            tbOutput.Clear();
        }

        private void btReset_Click(object sender, EventArgs e)
        {
            InitValue();
        }
    }
}