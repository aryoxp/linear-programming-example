namespace LinearProgramming
{
    partial class FormLinearProgramming
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btClear = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbX1 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbX2 = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btCalculate = new System.Windows.Forms.Button();
            this.radioAR2 = new System.Windows.Forms.RadioButton();
            this.radioAR1 = new System.Windows.Forms.RadioButton();
            this.radioAR0 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbY = new System.Windows.Forms.TextBox();
            this.tbOutput = new System.Windows.Forms.RichTextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tEquation = new System.Windows.Forms.Label();
            this.radioAR3 = new System.Windows.Forms.RadioButton();
            this.btReset = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(13, 13);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(393, 29);
            label1.TabIndex = 5;
            label1.Text = "Linear Regression Programming";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(18, 46);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(174, 13);
            label2.TabIndex = 6;
            label2.Text = "Aryo Pinandito | NRP. 9109205506";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tbOutput);
            this.groupBox1.Controls.Add(this.btClear);
            this.groupBox1.Location = new System.Drawing.Point(451, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(261, 375);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Debug Message";
            // 
            // btClear
            // 
            this.btClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClear.Location = new System.Drawing.Point(180, 346);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(75, 23);
            this.btClear.TabIndex = 4;
            this.btClear.Text = "Clear";
            this.btClear.UseVisualStyleBackColor = true;
            this.btClear.Click += new System.EventHandler(this.btClear_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbX1);
            this.groupBox3.Location = new System.Drawing.Point(162, 86);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(138, 219);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data X1";
            // 
            // tbX1
            // 
            this.tbX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbX1.Location = new System.Drawing.Point(6, 20);
            this.tbX1.Multiline = true;
            this.tbX1.Name = "tbX1";
            this.tbX1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbX1.Size = new System.Drawing.Size(126, 193);
            this.tbX1.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbX2);
            this.groupBox4.Location = new System.Drawing.Point(306, 86);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(138, 219);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data X2";
            // 
            // tbX2
            // 
            this.tbX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbX2.Location = new System.Drawing.Point(6, 20);
            this.tbX2.Multiline = true;
            this.tbX2.Name = "tbX2";
            this.tbX2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbX2.Size = new System.Drawing.Size(126, 193);
            this.tbX2.TabIndex = 2;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.radioAR3);
            this.groupBox5.Controls.Add(this.btCalculate);
            this.groupBox5.Controls.Add(this.radioAR2);
            this.groupBox5.Controls.Add(this.radioAR1);
            this.groupBox5.Controls.Add(this.radioAR0);
            this.groupBox5.Location = new System.Drawing.Point(18, 409);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(426, 52);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Auto Regression";
            // 
            // btCalculate
            // 
            this.btCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btCalculate.Location = new System.Drawing.Point(345, 20);
            this.btCalculate.Name = "btCalculate";
            this.btCalculate.Size = new System.Drawing.Size(75, 23);
            this.btCalculate.TabIndex = 3;
            this.btCalculate.Text = "Calculate";
            this.btCalculate.UseVisualStyleBackColor = true;
            this.btCalculate.Click += new System.EventHandler(this.btCalculate_Click);
            // 
            // radioAR2
            // 
            this.radioAR2.AutoSize = true;
            this.radioAR2.Location = new System.Drawing.Point(137, 23);
            this.radioAR2.Name = "radioAR2";
            this.radioAR2.Size = new System.Drawing.Size(53, 17);
            this.radioAR2.TabIndex = 2;
            this.radioAR2.Text = "AR(2)";
            this.radioAR2.UseVisualStyleBackColor = true;
            // 
            // radioAR1
            // 
            this.radioAR1.AutoSize = true;
            this.radioAR1.Location = new System.Drawing.Point(78, 23);
            this.radioAR1.Name = "radioAR1";
            this.radioAR1.Size = new System.Drawing.Size(53, 17);
            this.radioAR1.TabIndex = 1;
            this.radioAR1.Text = "AR(1)";
            this.radioAR1.UseVisualStyleBackColor = true;
            // 
            // radioAR0
            // 
            this.radioAR0.AutoSize = true;
            this.radioAR0.Checked = true;
            this.radioAR0.Location = new System.Drawing.Point(19, 23);
            this.radioAR0.Name = "radioAR0";
            this.radioAR0.Size = new System.Drawing.Size(53, 17);
            this.radioAR0.TabIndex = 0;
            this.radioAR0.TabStop = true;
            this.radioAR0.Text = "AR(0)";
            this.radioAR0.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbY);
            this.groupBox2.Location = new System.Drawing.Point(18, 86);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(138, 219);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data Y";
            // 
            // tbY
            // 
            this.tbY.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbY.Location = new System.Drawing.Point(6, 20);
            this.tbY.Multiline = true;
            this.tbY.Name = "tbY";
            this.tbY.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbY.Size = new System.Drawing.Size(126, 193);
            this.tbY.TabIndex = 1;
            // 
            // tbOutput
            // 
            this.tbOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOutput.Location = new System.Drawing.Point(6, 20);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.Size = new System.Drawing.Size(249, 320);
            this.tbOutput.TabIndex = 5;
            this.tbOutput.Text = "";
            this.tbOutput.WordWrap = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.tEquation);
            this.groupBox6.Location = new System.Drawing.Point(18, 346);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(426, 52);
            this.groupBox6.TabIndex = 13;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Regression Equation";
            // 
            // tEquation
            // 
            this.tEquation.AutoSize = true;
            this.tEquation.Location = new System.Drawing.Point(6, 25);
            this.tEquation.Name = "tEquation";
            this.tEquation.Size = new System.Drawing.Size(169, 13);
            this.tEquation.TabIndex = 0;
            this.tEquation.Text = "Linear regression equation is here";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(21, 308);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(50, 13);
            label3.TabIndex = 14;
            label3.Text = "Catatan:";
            // 
            // radioAR3
            // 
            this.radioAR3.AutoSize = true;
            this.radioAR3.Location = new System.Drawing.Point(196, 23);
            this.radioAR3.Name = "radioAR3";
            this.radioAR3.Size = new System.Drawing.Size(53, 17);
            this.radioAR3.TabIndex = 4;
            this.radioAR3.Text = "AR(3)";
            this.radioAR3.UseVisualStyleBackColor = true;
            // 
            // btReset
            // 
            this.btReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btReset.Location = new System.Drawing.Point(631, 36);
            this.btReset.Name = "btReset";
            this.btReset.Size = new System.Drawing.Size(75, 23);
            this.btReset.TabIndex = 15;
            this.btReset.Text = "Reset All";
            this.btReset.UseVisualStyleBackColor = true;
            this.btReset.Click += new System.EventHandler(this.btReset_Click);
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(77, 308);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(349, 13);
            label4.TabIndex = 16;
            label4.Text = "Data Y, X1, dan X2 adalah berpasangan dan memiliki jumlah data sama.";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(77, 325);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(168, 13);
            label5.TabIndex = 17;
            label5.Text = "Pisahkan antar data dengan baris";
            // 
            // FormLinearProgramming
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 473);
            this.Controls.Add(label5);
            this.Controls.Add(label4);
            this.Controls.Add(this.btReset);
            this.Controls.Add(label3);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormLinearProgramming";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Linear Programming";
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btClear;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioAR0;
        private System.Windows.Forms.RadioButton radioAR1;
        private System.Windows.Forms.RadioButton radioAR2;
        private System.Windows.Forms.TextBox tbX1;
        private System.Windows.Forms.TextBox tbX2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbY;
        private System.Windows.Forms.Button btCalculate;
        private System.Windows.Forms.RichTextBox tbOutput;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label tEquation;
        private System.Windows.Forms.RadioButton radioAR3;
        private System.Windows.Forms.Button btReset;
    }
}

